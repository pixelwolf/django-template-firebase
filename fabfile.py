#!/usr/bin/env python
# coding: utf-8

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# On Debian systems, you can find the full text of the license in
# /usr/share/common-licenses/GPL-2
#
# Developed By: Pixelwolf

import datetime
import os
import time
import sys

from fabric.api import env
from fabric.api import lcd
from fabric.api import local
from fabric.api import puts
from fabric.api import run

from config import ConfigFile


BASE_DIR = os.path.dirname(__file__)

env.app_root = BASE_DIR
env.project_settings_file = os.path.join(BASE_DIR, 'project.ini')


def _get_virtualenv_path():
    virtualenv_home = os.getenv('VIRTUALENV_HOME', None)

    if not virtualenv_home:
        return os.path.join(env.app_root, '.venv')

    return os.path.join(virtualenv_home, env.app_name)


def docker_compose(command):
    docker_compose_file = '-f '+'-f '.join(env.docker_compose_file)
    run('cd %s; docker-compose %s %s' % (env.app_root, docker_compose_file, command))


# - Available Environments
# ------------------------


def localhost():
    # get config file
    config = ConfigFile(env.project_settings_file)

    # local config
    env.app_name = config.project.name
    env.environment = 'localhost'
    env.virtualenv = _get_virtualenv_path()
    env.pip = os.path.join(env.virtualenv, "bin", "pip")
    env.python = os.path.join(env.virtualenv, "bin", "python")
    env.python_bin = '/usr/bin/python3'


def remote(server='stage', git_branch=None):
    """
    Set up the remote server configs.
    """
    # get config file
    config = ConfigFile(env.project_settings_file)

    # set the docker configs
    env.docker_containers = []
    env.docker_compose_file = ['docker-compose.prod.yml']

    # get server config
    server = config[server]

    # remote config
    env.app_name = config.project.name
    env.git_origin = config.project.git_origin
    env.git_branch = git_branch or server.git_branch
    env.app_root = server.project_dir
    env.user = server.server_user
    env.hosts = ['%s@%s' % (server.server_user, server.server_address)]


def dev(git_branch=None):
    """
    Compatibility mode.
    """
    remote(server='dev', git_branch=git_branch)


def prod(git_branch=None):
    """
    Compatibility mode.
    """
    remote(server='prod', git_branch=git_branch)


def stage(git_branch=None):
    """
    Compatibility mode.
    """
    remote(server='stage', git_branch=git_branch)


# - Available Commands
# --------------------


def provision():
    """
    Run the first setup on application.
    """

    if not hasattr(env, 'app_root'):
        sys.stdout.write('ERROR: unknown environment.')
        os.sys.exit(1)

    # starts provision
    start = time.time()

    if env.environment == 'localhost':
        # starts the localhost setup, install the project
        # requirements, run database migrations and any other
        # things you want to put your local environment up.
        sys.stdout.write('%(now)s\nStarting localhost setup...\n\n\n' % {
            'now': datetime.datetime.now().strftime('%A, %d. %B %Y %I:%M%p')
        })

        with lcd(env.app_root):

            if not os.path.exists(env.virtualenv):
                raise FileNotFoundError((
                    'Virtual environment on path \'%s\' wasn\'t created yet. '
                    'Please create your virtual environment before starts the provision.'
                    '\nYou can create your virtualenv with command: virtualenv -p /usr/bin/python3 %s'
                    '\nYou can create your virtualenvwrapper with command: mkvirtualenv %s -p /usr/bin/python3 -a %s'
                ) % (env.virtualenv, env.virtualenv, env.app_name, env.app_root))

            # install all project requirements
            command = '%s install -r %s'
            local(command % (env.pip, 'requirements.pip'))
            local(command % (env.pip, 'requirements-test.pip'))

            # wait until database is ready
            command = '%s wait_for_postgres.py'
            local(command % env.python)
    
            # applies django migrations
            command = '%s manage.py migrate'
            local(command % env.python)

            # clear directory
            command = 'find %s -name \'*.pyc\' -delete'
            local(command % env.app_root)

    final = time.time()
    puts('\nexecution finished in %.2fs' % (final - start))


def prepare():
    """
    Prepare a remote host to run the project.
    """
    start = time.time()

    # first of all create a ssh key on server
    # if it does not exist yet.
    command = 'test -e /home/%s/.ssh/id_rsa.pub || ssh-keygen -q -N ""'
    run(command % env.user)

    # shows the server public key.
    command = 'cat /home/%s/.ssh/id_rsa.pub; read -p "Press any key to continue... " -n1 -s'
    run(command % env.user)

    # grant app folder permissions
    command = 'sudo chown -R %s:%s /srv/'
    run(command % (env.user, env.user))

    # clone the project
    command = 'test -d %s/.git || git clone %s %s -b %s'
    run(command % (env.app_root, env.git_origin, env.app_root, env.git_branch))

    puts('Execution finished in %.2fs' % (time.time() - start))
    puts('Done.')


def deploy():
    start = time.time()
    containers = ' '.join(env.docker_containers)

    # update repository
    command = 'cd %s; git reset --hard && git pull && git checkout -B %s origin/%s '
    run(command % (env.app_root, env.git_branch, env.git_branch))

    # stop containers
    docker_compose('stop')

    # build containers
    docker_compose('build ' + containers)

    # build containers
    docker_compose('up -d ' + containers)

    puts(" ________")
    puts('< execution finished in %.2fs >' % (time.time() - start))
    puts(" ---------------------------------------")
    puts("                             .d$$b      ")
    puts("                           .' TO$;\     ")
    puts("                          /  : TP._;    ")
    puts("                         / _.;  :Tb|    ")
    puts("                        /   /   ;j$j    ")
    puts("                    _.-'       d$$$$    ")
    puts("                  .' ..       d$$$$;    ")
    puts("                 /  /P'      d$$$$P. |\ ")
    puts("                /   '      .d$$$P' |\^'l")
    puts("              .'           `T$P^'''''  :")
    puts("          ._.'      _.'                ;")
    puts("       `-.-'.-'-' ._.       _.-'    .-' ")
    puts("     `.-' _  ._              .-'        ")
    puts("    -(.g$$$$$$$b.              .'       ")
    puts("      ''^^T$$$P^)            .(:        ")
    puts("        _/  -'  /.'         /:/;        ")
    puts("     ._.'-'`-'  ')/         /;/;        ")
    puts("  `-.-'..--''   ' /         /  ;        ")
    puts(" .-' ..--''        -'          :        ")
    puts(" ..--''--.-'         (\      .-(\       ")
    puts("   ..--''              -\(\/;           ")
    puts("     _.                      :          ")
    puts("                             ;`-        ")
    puts("                            :\          ")
    puts("                            ;           ")
    puts(" ---------------------------------------")