# coding: utf-8
import os

from django.conf import settings


# Define the firebase credentials as
# an environment variable.

FIREBASE_CREDENTIALS = getattr(settings, 'FIREBASE_CREDENTIALS', None)


# Define the firebase credentials as
# a json file.

FIREBASE_CREDENTIALS_FILE = getattr(
    settings,
    'FIREBASE_CREDENTIALS_FILE',
    None) or os.path.join(settings.BASE_DIR, 'firebase-private-key.json')
