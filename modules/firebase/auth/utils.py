# coding: utf-8
from django.utils import six


def get_authorization_header(request):
    """
    Return request's 'Authorization:' header, as a byte-string.
    """
    auth = request.META.get('HTTP_AUTHORIZATION', b'')

    if isinstance(auth, six.text_type):
        # Work around django test client oddness
        auth = auth.encode('iso-8859-1')

    return auth


def split_name(name):
    """
    Returns the first and last name from a full name.
    """
    if not name:
        # in case of the name does not exist,
        # returns both as None.
        return None, None

    parts = name.split()

    # get the name from parts
    first_name = parts[0]
    last_name = ' '.join(parts[1:]) or None

    return first_name, last_name
