# coding: utf-8
import pyrebase

from django.utils.module_loading import import_string
from firebase_admin import auth as firebase
from requests import HTTPError

from ..base import get_firebase_app
from .settings import FIREBASE_AUTH_CLIENT
from .tools import get_firebase_user_model
from .tools.pyrebase_settings import PyRebaseSettings
from .utils import get_authorization_header


class FirebaseError(Exception):
    """
    This exception must be used to handle firebase exception
    through firebase api calls, such as login.
    """


class FirebaseAuthenticationBaseClient:
    """
    A simple helper to authenticate with firebase.
    """

    firebase_user_model = None

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # initialize the firebase app if it
        # was not initialized yet.
        self.app = get_firebase_app()

        # initialize firebase by pyRebase
        self.firebase = pyrebase.initialize_app(PyRebaseSettings.config())

    def authenticate(self, request):
        """
        Returns the authenticated user using the `Authorization`
        header to get the user token through the
        django request object.
        """
        auth = get_authorization_header(request).split()

        if not auth or auth[0].lower() != b'bearer' or len(auth) == 1:
            # if the authorization header has an invalid value,
            # refuse the authentication.
            return None

        # convert the token from byte to str.
        token = auth[1].decode('utf-8')

        # get the user throughout the token.
        user = self.authenticate_token(token)

        if not user:
            # in case of there is not a valid user for provided token,
            # just refuse the authentication.
            return None

        # otherwise, return the authenticated user
        # with the provided token.
        return user, token

    def authenticate_token(self, token):
        """
        Authenticate user using the provided token.

        Args:
             token: (string, required) - User token to be validated.

        Returns:
            user: Django user instance.
            null: User or Token is not valid.
        """
        try:

            # get the logged user information
            # if the token is valid.
            user_claims = firebase.verify_id_token(token)

            if not user_claims:
                # if the user was not found, raise an exception to tell it.
                raise firebase.AuthError(
                    code='invalid_user',
                    message='No user found with token \'{token}\''.format(
                        token=token
                    ))

        except firebase.AuthError:
            # invalid or expired credentials.
            user = None

        except ValueError:
            # invalid token format.
            user = None

        else:

            # get or create user on project.
            user, _ = self.model.get_or_create(
                uid=user_claims.get('uid'),
                name=user_claims.get('name'),
                email=user_claims.get('email')
            )

        return user

    def login(self, email, password):
        """
        Perform login using firebase api passing the
        email and password for api.

        This action must return a token and an error if exists
        any message to be reported to user.
        """
        auth = self.firebase.auth()

        try:
            credentials = auth.sign_in_with_email_and_password(email, password)

        except (firebase.AuthError, HTTPError) as exp:
            raise FirebaseError(exp)

        else:
            return credentials.get('idToken', '')

    def create_user(self, name, email, password):
        """
        This method should be used to create a firebase user,
        this user should not exist yet.

        In case of the user already exists, the user
        information will be returned.

        Args:
            name: (string, required) - User complete name.
            email: (string, required) - User email.
            password: (string, required) - Firebase password for user.

        Returns:
            tuple: (user, created) - returns a tuple with user
                information and created flag.
        """
        created = False

        # try to find the user on firebase.
        user = self.get_user_by_email(email)

        if not user:
            try:

                # try to create an user on firebase
                # with provided information.
                user = firebase.create_user(
                    display_name=name, email=email, password=password,
                    app=self.app)

                created = True

            except firebase.AuthError:

                # if was not possible to create the user for
                # any reason return None.
                return None, created

        # return user information.
        return {
            'uid': user.uid,
            'email': user.email,
            'name': user.display_name,
            'phone': user.phone_number,
            'avatar': user.photo_url
        }, created

    def get_user_by_email(self, email):
        """
        Try to find a user on firebase using
        his email address.
        """
        try:

            # try get the user by email.
            user = firebase.get_user_by_email(email=email, app=self.app)

        except firebase.AuthError:

            # in case of the user does not exist on firebase,
            # None will be returned.
            user = None

        return user

    @property
    def model(self):
        if not self.firebase_user_model:
            # if there is not a defined user model,
            # get the default user model.
            return get_firebase_user_model()

        # otherwise, returns the user model.
        return self.firebase_user_model


class FirebaseAuthenticationClient(FirebaseAuthenticationBaseClient):
    """
    Default Firebase Client.
    """


def get_firebase_auth_client():
    """
    Returns the project client defined from firebase.
    """
    return import_string(FIREBASE_AUTH_CLIENT)()


client = get_firebase_auth_client()
