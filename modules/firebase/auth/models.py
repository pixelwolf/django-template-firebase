# coding: utf-8
from django.db import models
from django.utils import timezone

from .utils import split_name


class FirebaseUser(models.Model):

    firebase_uid = models.CharField(
        'Firebase User ID', max_length=127,
        null=True, unique=True)

    # an option to choose which field will be used
    # to find the user.
    firebase_lookup_field = 'firebase_uid'
    firebase_lookup_suffix = None

    class Meta:
        abstract = True

    def save(self, **kwargs):
        """
        Grant that firebase_uid won't receive a blank value.
        """
        # avoids to receive blank values.
        self.firebase_uid = self.firebase_uid or None

        # force the email being lowercase.
        self.email = self.email.lower()

        super().save(**kwargs)

    @classmethod
    def get_firebase_lookup_field(cls, **kwargs):
        """
        Generate the query based on field and suffix.
        """
        lookup = cls.firebase_lookup_field

        if cls.firebase_lookup_suffix:
            lookup = '%s__%s' % (lookup, cls.firebase_lookup_suffix)

        return {
            lookup: kwargs.get(cls.firebase_lookup_field)
        }

    @classmethod
    def get_or_create(cls, uid, email, name=None, **kwargs):
        """
        Try to find a valid user from provided parameters,
        in case of the user does not exists yet,
        create him.

        Args:
            uid: (string, required) - Firebase user id.
            email: (string, required) - User email.
            name: (string, optional) - User complete name.

        Returns:
            tuple: (user, created) - The user object and a
                flag to tell if the user was created or not.
        """
        created = False

        if not uid:
            # Check if there is a valid user id
            # on the user data.
            return None, created

        try:
            # get lookup field to find the object.
            lookups = cls.get_firebase_lookup_field(**{
                **kwargs,
                'firebase_uid': uid,
                'email': email
            })

            # get the user based on defined lookup
            user = cls.objects.get(**lookups)
        except cls.DoesNotExist:
            created = True

            # get the user first and last names.
            first_name, last_name = split_name(name)

            user = cls.create(**{
                **kwargs,
                'uid': uid,
                'email': email,
                'first_name': first_name or email.split('@')[0],
                'last_name': last_name,
                'date_joined': timezone.now()
            })

        return user.get_user(), created

    @classmethod
    def create(cls, uid, email, first_name, last_name, **kwargs):
        """
        Each project could implement different information to users.
        So this method must be defined in each project.
        """
        raise NotImplementedError()

    def get_user(self):
        """
        Get related user object.
        """
        return self
