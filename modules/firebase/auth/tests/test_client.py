# coding: utf-8
from django.test import TestCase
from django.test.client import RequestFactory

from ..client import FirebaseAuthenticationClient
from ..settings import FIREBASE_AUTH_TEST_API_URL
from ..settings import FIREBASE_AUTH_TEST_USER_EMAIL
from ..settings import FIREBASE_AUTH_TEST_USER_PASSWORD
from ..tools import get_firebase_user_model


class TestFirebaseClient(TestCase):

    def setUp(self):
        """
        Get a valid firebase header.
        """

        # create a firebase client
        self.client = FirebaseAuthenticationClient()

        # create a request factory
        self.factory = RequestFactory()

    def test_client_initialization(self):
        """
        Test if client have an firebase client after initialization.
        """
        # Client should contains a firebase client
        self.assertIsNotNone(self.client.firebase)

    def test_login(self):
        """
        Test to get an valid token from firebase api using token.
        """
        token, err = self.client.login(FIREBASE_AUTH_TEST_USER_EMAIL, FIREBASE_AUTH_TEST_USER_PASSWORD)

        # check if there is no error
        self.assertIsNone(err)

        # check if there is a valid token
        self.assertIsNotNone(token)

    def test_authenticate(self):
        """
        Test firebase user getter function with valid header.
        """
        # get a valid token from firebase login.
        token = self.client.login(FIREBASE_AUTH_TEST_USER_EMAIL, FIREBASE_AUTH_TEST_USER_PASSWORD)[0]

        # create a fake request to authenticate user.
        request = self.factory.get(FIREBASE_AUTH_TEST_API_URL, HTTP_AUTHORIZATION='Bearer ' + token)

        # try to authenticate the user with this token
        user = self.client.authenticate(request)

        # check if the user was returned successfully
        self.assertIsNotNone(user)

        user_class = get_firebase_user_model()

        # check user creation
        user = user_class.objects.filter(email=FIREBASE_AUTH_TEST_USER_EMAIL).first()

        self.assertIsNotNone(user)

    def test_authentication_failure(self):
        """
        Test firebase user getter function with invalid header.
        """
        # get a invalid token.
        token = 'InvalidToken'

        # create a fake request to authenticate user.
        request = self.factory.get(FIREBASE_AUTH_TEST_API_URL, HTTP_AUTHORIZATION='Bearer ' + token)

        # try to authenticate the user with this token
        user = self.client.authenticate(request)

        # check if the user was returned successfully
        self.assertIsNone(user)
