"""
Firebase client test file.
"""
from rest_framework import status
from rest_framework.test import APITestCase

from ..settings import FIREBASE_AUTH_TEST_API_URL


class TestFirebaseAPIAuthentication(APITestCase):
    """
    Firebase Authentication test class.
    """

    def test_authorization_header_validation(self):
        """
        Test a simple api call to validate auth.
        """
        response = self.client.get(FIREBASE_AUTH_TEST_API_URL)

        # check the response status
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_unauthenticated_header_validation(self):
        """
        Test a simple api call to validate auth.
        """
        response = self.client.get(FIREBASE_AUTH_TEST_API_URL, **{
            'HTTP_AUTHORIZATION': 'It is definitely an invalid header!'
        })

        # check the response status
        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)
