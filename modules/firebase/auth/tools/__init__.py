# coding: utf-8
from django.apps import apps as django_apps
from django.core.exceptions import ImproperlyConfigured

from .pyrebase_settings import PyRebaseSettings
from ..settings import FIREBASE_AUTH_USER_MODEL


def get_firebase_user_model():
    """
    Return the fb_user_model model that is active in this project.
    """
    try:
        return django_apps.get_model(FIREBASE_AUTH_USER_MODEL, require_ready=False)
    except ValueError:
        raise ImproperlyConfigured(
            "FIREBASE_AUTH_USER_MODEL must be of the form 'app_label.model_name'"
        )
    except LookupError:
        raise ImproperlyConfigured(
            "FIREBASE_AUTH_USER_MODEL refers to model '%s' that has not been installed" % FIREBASE_AUTH_USER_MODEL
        )
