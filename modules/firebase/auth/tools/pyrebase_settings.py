import os
from django.conf import settings


class PyRebaseSettings:
    """
    Settings wrapper for PyRebase.
    """
    api_key = getattr(settings, 'FIREBASE_API_KEY', None)
    auth_domain = getattr(settings, 'FIREBASE_AUTH_DOMAIN', None)
    project_id = getattr(settings, 'FIREBASE_PROJECT_ID', None)
    storage_bucket = getattr(settings, 'FIREBASE_STORAGE_BUCKET', None)
    messaging_sender_id = getattr(settings, 'FIREBASE_MESSAGING_SENDER_ID', None)
    database_url = getattr(settings, 'FIREBASE_DATABASE_URL', None)

    @classmethod
    def config(cls):
        """
        Return configs as JSON for PyRebase initialization.
        """
        return {
            "apiKey": cls.api_key,
            "authDomain": cls.auth_domain,
            "databaseURL": cls.database_url,
            "storageBucket": cls.storage_bucket
        }
