# coding: utf-8
from django.conf import settings


# Define which url will be used to test the
# api connection.

FIREBASE_AUTH_TEST_API_URL = getattr(
    settings,
    'FIREBASE_AUTH_TEST_API_URL',
    None)


# Define an user email to login and test connection
# with firebase.

FIREBASE_AUTH_TEST_USER_EMAIL = getattr(
    settings,
    'FIREBASE_AUTH_TEST_USER_EMAIL',
    None)


# Define an user password to login and test connection
# with firebase.

FIREBASE_AUTH_TEST_USER_PASSWORD = getattr(
    settings,
    'FIREBASE_AUTH_TEST_USER_PASSWORD',
    None)


# Define with client will be used to
# authenticate the users user.

FIREBASE_AUTH_CLIENT = getattr(
    settings,
    'FIREBASE_AUTH_CLIENT',
    'modules.firebase.auth.client.FirebaseAuthenticationClient')


# Define which model will be used to
# get the user from firebase.

FIREBASE_AUTH_USER_MODEL = getattr(
    settings,
    'FIREBASE_AUTH_USER_MODEL',
    settings.AUTH_USER_MODEL)
