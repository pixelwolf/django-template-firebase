# coding: utf-8
from rest_framework import authentication

from .client import get_firebase_auth_client, get_authorization_header
from .settings import FIREBASE_AUTH_TEST_USER_EMAIL
from .tools import get_firebase_user_model


class FirebaseAuthentication(authentication.BaseAuthentication):

    def authenticate(self, request):
        """
        Uses firebase client to authenticate a
        user based on a request token.
        """
        client = get_firebase_auth_client()

        # Returns the user, token or null.
        return client.authenticate(request)

    def authenticate_header(self, request):
        """
        Return a string to be used as the value of the `WWW-Authenticate`
        header in a `401 Unauthenticated` response, or `None` if the
        authentication scheme should return `403 Permission Denied` responses.
        """
        return 'WWW-Authenticate'


class FirebaseTestAuthentication(authentication.BaseAuthentication):
    """
    Custom authenticator to use for tests only.

    WARNING!
    Don't use this authenticator on production environment, this is to use only
    for testing purposes!

    How to install:
    Add this authenticator on DRF default authentication classes. It's
    recommended to add a custom settings template for testing and set this
    authenticator on the separated settings template.

    How to use:
    To authenticate add a header with a fake jwt token like the example:
    Authorization: Bearer xxxx.yyyy.zzzz
    The authenticator only authorize the token above exact as is.
    This token will log the user especified on the file using the settings
    located bellow on the same file, named as FIREBASE_UID and EMAIL.
    """

    firebase_user_id = '1234'
    firebase_user_email = FIREBASE_AUTH_TEST_USER_EMAIL
    firebase_user_name = 'Pixelwolf Developer'

    default_bearer_authorization = b'Bearer xxxx.yyyy.dev'

    def get_or_create_user(self):
        """
        Retrieve or Create the default test user.

        Returns:
            User object.
        """
        user_model = get_firebase_user_model()

        try:

            dj_firebase_user = user_model.objects.get(email=self.firebase_user_email)

        except user_model.DoesNotExist:
            names = self.firebase_user_name.split()

            dj_firebase_user = user_model.create(
                uid=self.firebase_user_id,
                email=self.firebase_user_email,
                first_name=names[0],
                last_name=names[1]
            )

        return dj_firebase_user.get_user()

    def get_user_from_token(self, token):
        """
        Returns an email from token.

        Args:
          token (string, required) - Token from authorization header.

        Returns:
          string - Extracted email from token.
        """
        token = token.split()

        if not token or token[0].lower() != b'email' or len(token) == 1:
            # if the authorization header has an invalid value,
            # refuse the authentication.
            return None

        # convert the token from byte to str.
        email = token[1].decode('utf-8')

        user_model = get_firebase_user_model()

        try:
            user = user_model.objects.get(email=email)
        except user_model.DoesNotExist:
            return None

        return user.get_user()

    def authenticate(self, request):
        """
        Authenticate method to work as a BackendAuthentication.
        """
        auth = get_authorization_header(request) or self.default_bearer_authorization

        if auth == self.default_bearer_authorization:
            user = self.get_or_create_user()
            return user, auth

        # if it is not the default user, try to
        # get the user with provided email in token.
        # HTTP_AUTHORIZATION: Email {email}
        user = self.get_user_from_token(auth)

        if user is not None:
            return user, auth

        return None

    def authenticate_header(self, request):
        """
        Return a string to be used as the value of the `WWW-Authenticate`
        header in a `401 Unauthenticated` response, or `None` if the
        authentication scheme should return `403 Permission Denied` responses.
        """
        return 'WWW-Authenticate'
