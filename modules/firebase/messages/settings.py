# coding: utf-8
from django.conf import settings


# Define with client will be used to
# authenticate the users user.

FIREBASE_MESSAGES_CLIENT = getattr(
    settings,
    'FIREBASE_MESSAGES_CLIENT',
    'modules.firebase.messages.client.FirebaseMessageClient')
