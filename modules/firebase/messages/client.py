# coding: utf-8
import logging

from django.dispatch import Signal
from django.utils.module_loading import import_string
from firebase_admin import messaging

from .settings import FIREBASE_MESSAGES_CLIENT
from ..base import get_firebase_app


firebase_post_message_send = Signal(providing_args=['message', 'response', 'error'])


logger = logging.getLogger('firebase.messages')


class FirebaseMessageBaseClient:
    """
    A simple helper to send notifications with firebase.
    """

    post_send_signal = firebase_post_message_send

    # define a sender
    sender = messaging

    def __init__(self):

        # initialize the firebase app if it
        # was not initialized yet.
        self.app = get_firebase_app()

    def send(self, message, debug=False):
        """
        Send a message to firebase.
        """
        try:

            # send the message to firebase cloud.
            return self.sender.send(message, dry_run=debug), None

        except self.sender.ApiCallError as exception:
            msg = 'Erro ao enviar notificação %s com mensagem %s' % (
                message.notification.title,
                exception
            )

            # log error message.
            logger.error(msg)

            # In case of occurs an error returns a
            # sent flag as False.
            return None, str(exception)

    def send_to_topic(self, topic, title, content, payload=None, debug=False):
        """
        Send a notification to a specific topic.
        """
        # create a firebase message.
        message = messaging.Message(
            notification=messaging.Notification(title=title, body=content),
            topic=topic, data=payload or {})

        # send the message and get the response
        response, error = self.send(message, debug=debug)

        # call the signals after send the message
        self.post_send_signal.send(
            'topic',
            message=message,
            response=response,
            error=error
        )

    def send_to_device(self, token, title, content, payload=None, debug=False):
        """
        Send a notification to a specific topic.
        """
        if not isinstance(token, (list, tuple)):
            # grant that token will be treated as a list.
            token = [token]

        for device_token in token:
            # create a firebase message.
            message = messaging.Message(
                notification=messaging.Notification(title=title, body=content),
                token=device_token, data=payload or {})

            # send the message and get the response
            response, error = self.send(message, debug=debug)

            # call the signals after send the message
            self.post_send_signal.send(
                'token',
                message=message,
                response=response,
                error=error)


class FirebaseMessageClient(FirebaseMessageBaseClient):
    """
    Default Firebase Message Client.
    """


def get_firebase_message_client():
    """
    Returns the project client defined from firebase.
    """
    return import_string(FIREBASE_MESSAGES_CLIENT)()
