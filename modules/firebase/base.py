# coding: utf-8
import firebase_admin
import json
import os

from firebase_admin.auth import AuthError

from .settings import FIREBASE_CREDENTIALS
from .settings import FIREBASE_CREDENTIALS_FILE


def singleton(kls):

    class MetaWrapper(type(kls)):
        """
        Overload the __call__ function in metaclass
        """
        def __init__(self, name, bases, attrs):
            super().__init__(name, bases, attrs)
            self._instance = None

        def __call__(self, *args, **kwargs):
            if not self._instance:

                # create a new instance based on the init arguments.
                self._instance = super().__call__(*args, **kwargs)

            # returns the created instance
            return self._instance

    class Wrapper(kls, metaclass=MetaWrapper):
        pass

    Wrapper.__name__ = kls.__name__
    Wrapper.__repr__ = kls.__repr__

    return Wrapper


@singleton
class FirebaseApp:

    def __init__(self):
        try:
            # initialize the firebase app on the startup.
            self.app = firebase_admin.initialize_app(self.credential)

        except ValueError as exception:
            print("Error initializing firebase_admin", exception)

        except AuthError as exception:
            print("Error initializing firebase_admin", exception)

    @property
    def credential(self):
        """
        Returns a firebase credentials applying the credentials
        provided in django settings.
        """
        credentials = None

        if FIREBASE_CREDENTIALS:
            credentials = json.loads(FIREBASE_CREDENTIALS)
        elif os.path.exists(FIREBASE_CREDENTIALS_FILE):
            with open(FIREBASE_CREDENTIALS_FILE, 'r') as f:
                credentials = json.loads(f.read())

        return firebase_admin.credentials.Certificate(credentials)


def get_firebase_app():
    """
    Initialize a firebase app.
    """
    firebase_app = FirebaseApp()

    # returns the initialized app.
    return firebase_app.app
