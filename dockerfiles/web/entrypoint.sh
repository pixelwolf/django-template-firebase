#!/bin/bash
set -e

# wait until postgres is ready
python utilities/wait_for_postgres.py

# build admin styles
python manage.py compileadmincss

# execute django
python manage.py migrate --noinput
python manage.py collectstatic --noinput

# start gunicorn
gunicorn --workers=${SERVER_WORKERS:-5} --max-requests=${SERVER_WORKERS:-1200} --timeout=${SERVER_TIMEOUT:-120} --bind=0.0.0.0:8000 core.wsgi:application

exec "$@"
