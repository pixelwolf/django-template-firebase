"""{{ project_name }} URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, include

from apps.pxadmin.utils import admin_url_pattern


urlpatterns = [
    path('', include('apps.pxadmin.urls')),
    path('api/', include('apps.api.urls'))
]


# Apply the media and static files urls.
# It works only when is in debug mode.
urlpatterns += static(settings.MEDIA_URL, document_root=getattr(settings, 'MEDIA_ROOT'))
urlpatterns += static(settings.STATIC_URL, document_root=getattr(settings, 'STATIC_ROOT'))
