# coding: utf-8
import os

from django.contrib.auth import get_user_model
from django.db.models.signals import post_migrate
from django.dispatch import receiver


@receiver(post_migrate)
def add_super_admin_signal(app_config, *args, **kwargs):
    """
    Add super user on first syncdb.
    """
    User = get_user_model()

    if app_config.label == 'auth' and not User.objects.filter(is_superuser=True).exists():
        name = os.environ.get('SUPERUSER_NAME', default='Pixelwolf Webmaster')
        email = os.environ.get('SUPERUSER_EMAIL', default='webmaster@pixelwolf.com.br')
        password = os.environ.get('SUPERUSER_PASSWORD', default='Pxmatilh4!')

        # create user.
        User.objects.create_superuser(email=email, name=name, password=password)
