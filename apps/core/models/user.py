# coding: UTF-8
from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _

from apps.utils.models.mixins.audit import AuditMixin
from modules.firebase.auth.models import FirebaseUser
from modules.firebase.auth.utils import split_name


class UserManager(BaseUserManager):
    """
    Apply a custom creation from
    custom users on this project.
    """

    def create_superuser(self, email, name, password, *args, **kwargs):
        """
        Create a superuser on project.
        """
        user = self.create(email=email, name=name, is_superuser=True, *args, **kwargs)

        # set the user password.
        user.set_password(password)
        user.save()

        return user


class User(AbstractBaseUser, FirebaseUser, PermissionsMixin, AuditMixin):
    """
    User class definition.
    """
    STATUS_ACTIVE = 1
    STATUS_INACTIVE = 2

    name = models.CharField(
        _('Name'), max_length=120)

    email = models.EmailField(
        _('Email'), unique=True)

    date_joined = models.DateTimeField(
        _('Date Joined'), default=timezone.now)

    is_superuser = models.BooleanField(
        _('Superuser'), default=False)

    status = models.PositiveSmallIntegerField(
        _('Status'), default=STATUS_ACTIVE,
        choices=(
            (STATUS_ACTIVE, _('Active')),
            (STATUS_INACTIVE, _('Inactive')),
        ))

    # Django user settings

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    # Managers
    objects = UserManager()

    class Meta:
        verbose_name = _('User')
        verbose_name_plural = _('Users')
        ordering = ['name']

    def __str__(self):
        return self.get_full_name()

    @property
    def is_staff(self):
        """
        If the user is superuser it has totally access to admin site.
        """
        return self.is_superuser

    @property
    def is_active(self):
        """
        Defines that the user is able to perform actions.
        """
        return self.status == User.STATUS_ACTIVE

    @property
    def first_name(self):
        """
        Returns the user first name.
        """
        # get the first name based on the user name.
        first_name, _ = split_name(self.name)

        return first_name

    @property
    def last_name(self):
        """
        Returns the user first name.
        """
        # get the last name based on the user name.
        _, last_name = split_name(self.name)

        return last_name

    def get_full_name(self):
        """
        Returns the user complete name.
        """
        return self.name

    def get_short_name(self):
        """
        Returns the user short name.
        """
        return self.first_name

    @staticmethod
    def create(firebase_uid, email, date_joined=None, **kwargs):
        """
        Create a new firebase related user and returns it.
        """
        date_joined = date_joined or timezone.now()

        return User.objects.create(
            email=email,
            firebase_uid=firebase_uid,
            date_joined=date_joined,
            **kwargs)
