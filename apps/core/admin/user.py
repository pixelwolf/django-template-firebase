# coding: utf-8
from django.contrib import admin
from django.contrib.auth.models import Group

from apps.core import models
from apps.pxadmin.admin import SmartModelAdmin


# Unregister group admin.
admin.site.unregister(Group)


@admin.register(models.User)
class UserAdmin(SmartModelAdmin):
    list_display = ['name', 'email', 'date_joined', 'status']
    list_filter = ['status', 'last_login']
    search_fields = ['name', 'email']
    date_hierarchy = 'date_joined'

    readonly_fields = ['name', 'email', 'date_joined', 'status']

    fieldsets = (
        (None, {
            'fields': ['name', 'email', 'date_joined', 'status']
        }),
    )

    def get_queryset(self, request):
        """
        Remove all super user from user administration.
        """
        return super().get_queryset(request).filter(is_superuser=False)

    # - Permissions

    def has_add_permission(self, request):
        """
        Grant that nobody can add a user.
        """
        return False

    def has_delete_permission(self, request, obj=None):
        """
        Grant access only to super users.
        """
        return request.user.is_superuser

    def has_change_permission(self, request, obj=None):
        """
        Grant access only to super users.
        """
        return request.user.is_superuser

    def has_module_permission(self, request):
        """
        Grant access only to super users.
        """
        return request.user.is_superuser
