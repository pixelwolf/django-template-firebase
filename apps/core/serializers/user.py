# coding: utf-8
from rest_framework import serializers

from apps.core import models


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.User
        fields = ['name', 'email', 'date_joined']
