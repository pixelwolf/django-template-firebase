# coding: utf-8
from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from apps.core import models
from modules.firebase.auth.client import get_firebase_auth_client


class AuthTokenSerializer(serializers.Serializer):

    email = serializers.EmailField(label=_('Email'))

    password = serializers.CharField(
        label=_('Password'),
        style={'input_type': 'password'},
        trim_whitespace=False)

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')
        client = get_firebase_auth_client()

        if email and password:

            if not models.User.objects.filter(email=email).exists():
                msg = _('User with email %(email)s does not exist.') % {'email': email}
                raise serializers.ValidationError({'email': msg}, code='authorization')

            token, err = client.login(email, password)
            attrs['token'] = token

            if not token and err:
                msg = _('It is not possible to login with provided credentials')
                raise serializers.ValidationError({'email': msg}, code='authorization')
        else:
            msg = _('It is necessary to provide a valid email and password to login.')
            raise serializers.ValidationError({'email': msg}, code='authorization')

        return attrs

    def create(self, validated_data):
        """
        Ignore this method.
        """

    def update(self, instance, validated_data):
        """
        Ignore this method.
        """
