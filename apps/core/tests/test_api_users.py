# coding: utf-8
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from apps.utils.schema import TestSchemaMixin


class UserApiTestCase(APITestCase, TestSchemaMixin):

    user_scheme = {
        'type': 'object',
        'properties': {
            'name': {'type': 'string'},
            'email': {
                'type': 'string',
                'format': 'email'
            },
            'date_joined': {
                'type': 'string',
                'format': 'date-time'
            },
        },
        'required': ['name', 'email', 'date_joined'],
        'additionalProperties': False
    }

    def test_users_me_api(self):
        """
        Test a request to users api and check if the returns is right.
        """
        response = self.client.get(reverse('api:users-me'))

        # check if the response is ok
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # check the returned value
        self.assertSchema(self.user_scheme, response.json())
