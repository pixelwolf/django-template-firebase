# coding: utf-8
from rest_framework import views, mixins, status
from rest_framework.response import Response

from apps.core.serializers.user import UserSerializer


class UserMeDetailViewSet(mixins.RetrieveModelMixin, views.APIView):
    serializer_class = UserSerializer

    def get_object(self):
        """
        Returns the current logged user to be serialized.
        """
        return self.request.user

    def get(self, request, *args, **kwargs):
        """
        Returns the current user serialized.
        """
        obj = self.get_object()

        return Response(
            self.serializer_class(obj).data,
            status=status.HTTP_200_OK)
