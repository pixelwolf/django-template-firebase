# coding: utf-8
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.core.serializers.auth import AuthTokenSerializer


class AuthenticationView(APIView):
    serializer_class = serializer = AuthTokenSerializer
    authentication_classes = []
    permission_classes = []

    def post(self, request):
        """
        Provides the ability to get a token from user in firebase,
        using user email and password.

        Commonly used to authenticate Postman and use api.
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(
            data={'token': serializer.validated_data['token']},
            status=status.HTTP_200_OK)
