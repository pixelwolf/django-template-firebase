# coding: utf-8

from django.urls import path, include


urlpatterns = [
    # include api urls
    path('api/v1/', include('apps.core.urls.api')),
]
