# coding: utf-8
from django.urls import path

from apps.core.viewsets.auth import AuthenticationView


urlpatterns = [
    path('auth/', AuthenticationView.as_view(), name='auth'),
]
