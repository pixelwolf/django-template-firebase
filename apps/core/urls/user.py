# coding: utf-8
from django.urls import path
from apps.core.viewsets.user import UserMeDetailViewSet


urlpatterns = [
    path('users/me/', UserMeDetailViewSet.as_view(), name='users-me'),
]
