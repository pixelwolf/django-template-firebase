# coding: utf-8

from django.urls import path, include


app_name = 'api'


urlpatterns = [
    path('', include('apps.core.urls.user')),
    path('', include('apps.core.urls.auth')),
]
