# coding: utf-8
import inspect

from functools import update_wrapper

from django.urls import path
from django.utils.html import format_html_join
from django.utils.module_loading import import_string
from django.utils.safestring import mark_safe



class AdminViewMixin:

    extra_views = []

    def get_extra_views(self):
        """
        Walk into current class and get all extra views.
        """
        if self.extra_views:
            # if the extra view was defined manually,
            # consider it's more important than walking
            # in the class to discovery views.
            return [getattr(self, view_name) for view_name in self.extra_views]

        extra_views = []

        for view_name in filter(lambda x: x.endswith('_view'), dir(self)):
            # get only views names with the suffix `_view`.
            view_func = getattr(self, view_name)

            if not getattr(view_func, 'is_extra_view', False):
                # ignores if the function was not wrapped
                # by @admin_view decorator.
                continue

            extra_views.append(view_func)

        return extra_views

    def get_extra_view(self, view):
        """
        Handle the current view to add custom
        information to register in
        admin site urls.
        """
        # resolve the current view name.
        name = getattr(view, 'name', None) or view.__name__.replace('_view', '').replace('_', '-')

        # resolve the current view route.
        route = getattr(view, 'route', None) or ('%s/' % name)

        if getattr(view, 'detail', False):
            # add object_id argument if the view is a detail view.
            route = '<path:object_id>/{route}/'.format(route=route.rstrip('/').lstrip('/'))

        def wrap(func):
            """
            Wraps current view as admin view.
            """
            def wrapper(*args, **kwargs):
                return self.admin_site.admin_view(view)(*args, **kwargs)

            # define current admin as the model admin instance.
            wrapper.model_admin = self

            # returns the wrapped view
            return update_wrapper(wrapper, func)

        # returns the wrapped view, name and route
        # from this routes.
        return wrap(view), name, route

    def get_extra_urls(self):
        opts = getattr(self.model, '_meta')
        app_label = opts.app_label
        model_name = opts.model_name

        urlpatterns = []

        for view in self.get_extra_views():
            view, name, route = self.get_extra_view(view)

            urlpatterns.append(
                path(route, view, name='%s_%s_%s' % (app_label, model_name, name))
            )

        return urlpatterns

    def get_urls(self):
        urlpatterns = super(AdminViewMixin, self).get_urls()
        extra_urlpatterns = self.get_extra_urls()
        return extra_urlpatterns + urlpatterns


class AdminActionMixin:

    def get_extra_actions(self):
        """
        Walk through the class methods to identify
        which are extra actions and
        return them.
        """
        actions = []

        for name in filter(lambda x: x.endswith('_action'), dir(self)):
            # get only actions names with the suffix `_action`.
            func = getattr(self, name)

            if not getattr(func, 'is_extra_action', False):
                # ignores if the function was not wrapped
                # by @admin_action decorator.
                continue

            actions.append((func, name, func.short_description))

        return actions

    def _get_base_actions(self):
        """
        Return both base actions and extra actions.
        """
        actions = super()._get_base_actions()

        # join base actions and extra actions.
        return list(actions) + self.get_extra_actions()


class AdminToolsMixin:
    """
    A mixin to provide the object tools buttons.

    Ex:
    >>> object_tools = {
    >>>     'changelist': [{
    >>>         'title': 'Button Title',
    >>>         'url': 'url/action/',
    >>>         'permission': 'path.to.permission.checker',
    >>>         'attrs': {'attr1': 'value'}
    >>>     }, {
    >>>         'title': 'Button Title',
    >>>         'url': lambda opts: 'url/action/',
    >>>         'permission': 'path.to.permission.checker',
    >>>         'attrs': {'attr1': 'value'}
    >>>     }],
    >>>     'change': [{
    >>>         'title': 'Button Title',
    >>>         'url': lambda opts, obj: 'url/action',
    >>>         'permission': 'path.to.permission.checker',
    >>>         'attrs': {'attr1': 'value'}
    >>>     }]
    >>> }
    """
    object_tools = None

    def has_tool_permission(self, permission, request, obj=None):
        """
        Check if the permission was granted.
        """
        if not permission:
            return True

        if not callable(permission):
            # try to get the permission
            permission = import_string(permission)

        params = {}

        # get the expected args of the function
        args = inspect.getfullargspec(permission).args

        if 'request' in args:
            # add request if required
            params['request'] = request

        if 'obj' in args:
            # add obj if required
            params['obj'] = obj

        return permission(**params)

    def get_tool_title(self, title):
        """
        Returns the title based on model meta information.
        """
        opts = getattr(self.model, '_meta')

        return title % {
            'verbose_name': opts.verbose_name,
            'verbose_name_plural': opts.verbose_name_plural,
            'app_label': opts.app_label
        }

    def render_group(self, request, title, items, attrs=None, obj=None):
        """
        Render the whole group of tools.
        """
        tpl = (
            '<li class="dropdown"><a title="{title}" data-toggle="dropdown" href="#"{attrs}>{title}</a>'
            '<ul class="dropdown-menu">{items}</ul></li>'
        )

        # render all items
        rendered_tools = filter(None, [self.render_tool(request, obj=obj, **tool) for tool in items])

        if not rendered_tools:
            # if any item has permission or the list is empty,
            # then ignore this tool.
            return None

        attrs = attrs or {}

        return tpl.format(
            title=self.get_tool_title(title),
            attrs=format_html_join('', ' {}="{}"', attrs.items()),
            items=''.join(list(rendered_tools))
        )

    def render_tool(self, request, title, url, permission=None, attrs=None, obj=None):
        """
        Returns a rendered tool button based on the data.
        """
        tpl = '<li><a title="{title}" href="{url}"{attrs}>{title}</a></li>'

        opts = getattr(self.model, '_meta')

        # if has no permission return None
        if not self.has_tool_permission(permission, request, obj):
            return None

        if callable(url):
            # if the url is callable try to call the
            # function to get the real url.
            params = {'opts': opts}

            if obj is not None:
                params['obj'] = obj

            try:
                url = url(**params)
            except TypeError:
                return ''

        attrs = attrs or {}

        return tpl.format(
            title=self.get_tool_title(title),
            url=url,
            attrs=format_html_join('', ' {}="{}"', attrs.items())
        )

    def get_object_tools(self, request, view_name, obj=None):
        """
        Returns a object tool list for required view.
        """
        output = []

        if not self.object_tools:
            return output

        # get all tools for view.
        tools = self.object_tools.get(view_name, [])

        # Render each tool according with type and permission.
        rendered_tools = []

        for tool in tools:
            if 'items' in tool:
                rendered_tool = self.render_group(request, obj=obj, **tool)
            else:
                rendered_tool = self.render_tool(request, obj=obj, **tool)

            if not rendered_tool:
                continue

            rendered_tools.append(mark_safe(rendered_tool))

        # returns only rendered tools which
        # has permission.
        return rendered_tools

    def changelist_view(self, request, extra_context=None):
        """
        Add the changelist object tools to context.
        """
        extra_context = {
            **(extra_context or {}),
            'object_tools': self.get_object_tools(request, 'changelist')
        }

        return super(AdminToolsMixin, self).changelist_view(request, extra_context)

    def changeform_view(self, request, object_id=None, form_url='', extra_context=None):
        """
        Add the changeform object tools to context.
        """
        if object_id is not None:
            # if there is a object id, it means that the user
            # want to edit some object in administration.
            obj = self.get_object(request, object_id)

            extra_context = {
                **(extra_context or {}),
                'object_tools': self.get_object_tools(request, 'change', obj)
            }

        return super(AdminToolsMixin, self).changeform_view(request, object_id, form_url, extra_context)
