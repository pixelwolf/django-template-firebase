# coding: utf-8
import inspect
import itertools

from django import template as django_template
from django.conf import settings
from django.template.base import Node, TemplateSyntaxError, token_kwargs
from django.utils import six
from django.utils.module_loading import import_string

from apps.pxadmin.settings import ADMIN_USER_LINKS
from apps.pxadmin.shortcuts import admin_shortcuts
from apps.pxadmin.utils import querystring


register = django_template.Library()


@register.simple_tag(name='server')
def do_server(*args, **kwargs):
    """
    Returns the current application version.
    """
    # format and returns this version
    return getattr(settings, 'ENVIRONMENT', 'development')


@register.simple_tag(name='version')
def do_version(*args, **kwargs):
    """
    Returns the current application version.
    """
    # get version from settings
    version = getattr(settings, 'VERSION', '1.0.0')

    # get current server
    server = str(getattr(settings, 'ENVIRONMENT', 'development')).upper()

    # format and returns this version
    return f'v{version} ⋅ {server}'


@register.inclusion_tag('admin/tags/dummy.html', name='admin_shortcuts', takes_context=True)
def do_admin_shortcuts(context, template='admin/tags/shortcuts.html'):
    """
    Returns a rendered template with admin shortcuts.
    """
    request = context.get('request')

    return {
        'template': template,
        'shortcuts': admin_shortcuts(request),
        'request': request
    }


@register.inclusion_tag('admin/tags/dummy.html', name='changelist_widget', takes_context=True)
def do_changelist_widget(context, widget):
    """
    Render all widget in a list.
    """
    request = context.get('request')

    return {
        **widget,
        'request': request
    }


@register.simple_tag(name='get_user_links', takes_context=True)
def do_get_user_links(context):
    """
    Return all user links.
    """
    links = []

    for link in (ADMIN_USER_LINKS or []):
        permission = link.get('permission')

        if isinstance(permission, six.string_types):
            permission = import_string(permission)

        if callable(permission):
            func = inspect.getfullargspec(permission)
            kwargs = {}

            if 'request' in func.args:
                kwargs['request'] = context.get('request')

            if not permission(**kwargs):
                continue

        links.append(link)

    return links


class QuerystringNode(Node):
    def __init__(self, url, includes, excludes):
        self.url = url
        self.includes = includes
        self.excludes = excludes

    def resolve(self, value, context):
        """
        Resolve a single value.
        """
        return value.resolve(context)

    def resolve_kwargs(self, values, context):
        """
        Resolve dict based values.
        """
        return {key: self.resolve(value, context) for key, value in values.items()}

    def resolve_args(self, values, context):
        """
        Resolve list based values.
        """
        return [self.resolve(value, context) for value in values]

    def render(self, context):
        """
        Returns a modified URL based on include and exclude parameters.
        """
        return querystring(
            url=self.resolve(self.url, context),
            includes=self.resolve_kwargs(self.includes, context),
            excludes=self.resolve_args(self.excludes, context)
        )


@register.tag('querystring')
def do_querystring(parser, token):
    """
    Modify a URL including or excluding querystring parameters.

    >>> querystring '/some/url/?next=1' includes page=1 excludes 'next'
    """
    bits = token.split_contents()

    if len(bits) < 2:
        raise TemplateSyntaxError("'%s' need url argument." % bits[0])

    includes, excludes = {}, []
    url = parser.compile_filter(bits[1])
    bits = bits[2:]

    if 'includes' in bits and 'excludes' in bits:
        bits = itertools.groupby(bits, lambda x: x in ['includes', 'excludes'])
        includes, excludes = [list(y) for x, y in bits if not x]
    elif 'includes' in bits and 'excludes' not in bits:
        bits = itertools.groupby(bits, lambda z: z == 'includes')
        includes = [list(y) for x, y in bits if not x][0]
    elif 'excludes' in bits and 'includes' not in bits:
        bits = itertools.groupby(bits, lambda z: z == 'excludes')
        excludes = [list(y) for x, y in bits if not x][0]

    includes = token_kwargs(includes, parser, support_legacy=False)
    excludes = [parser.compile_filter(value) for value in excludes]

    return QuerystringNode(url, includes, excludes)
