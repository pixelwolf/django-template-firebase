# coding: utf-8
from urllib.parse import urljoin

from django import forms
from django.contrib.admin import widgets
from django.core.exceptions import ImproperlyConfigured
from django.forms import Media


class AutoCompleteSelectWidget(forms.Select):
    """
    Select widget with autocomplete based on
    select2 jquery widget and ajax.
    """
    template_name = 'forms/widgets/auto_complete.html'

    def __init__(self, url, minimum_length=3, attrs=None, choices=()):
        attrs = dict(attrs or {}, style="width: 275px;")
        super(AutoCompleteSelectWidget, self).__init__(attrs=attrs, choices=choices)

        # url to get options
        self.url = url

        # setting up the minimum chars to
        # types before search
        self.minimum_length = minimum_length

    def get_context(self, name, value, attrs):
        """
        Add extra arguments to context.
        """
        context = super(AutoCompleteSelectWidget, self).get_context(name, value, attrs)

        context['widget']['url'] = self.url
        context['widget']['minimum_input_length'] = self.minimum_length
        return context

    @property
    def media(self):
        """
        AutocompleteSelectMultipleWidget's Media.
        """
        return Media(
            js=['admin/select2/select2.min.js'],
            css={
                'all': ['admin/select2/select2.min.css']
            }
        )


class PartialUrlInput(widgets.AdminURLFieldWidget):
    """
    This can be used to render a url input
    with the root path defined.
    """
    input_type = 'text'
    template_name = 'forms/widgets/partial_url.html'

    def __init__(self, attrs=None, root_url=None):
        # define the root url for the component.
        self.root_url = root_url

        super().__init__(attrs={'class': 'input vURLField', **(attrs or {})})

    def get_root_url(self, value):
        """
        Try to return a valid root url.
        """
        if not self.root_url:
            raise ImproperlyConfigured('The \'root_url\' must be provided to use PartialUrlInput.')

        return self.root_url

    def format_value(self, value):
        """
        Return a value as it should appear when rendered in a template.
        """
        value = super(PartialUrlInput, self).format_value(value)

        if not value:
            # ignore blank and null values
            return None

        return value.replace(self.get_root_url(value), '')

    def get_context(self, name, value, attrs):
        context = super().get_context(name, value, attrs)

        # get the original value
        original_value = urljoin(self.get_root_url(value), value)

        # add the root url to context before render the widget.
        context['root_url'] = self.get_root_url(value)
        context['widget']['href'] = original_value
        context['widget']['original'] = original_value

        # return the context.
        return context


class ColorPickerInput(forms.TextInput):
    """
    This widget can be used to render a color picker input
    """
    def __init__(self, attrs=None):
        # set the color picker class and
        attrs = {
            **(attrs or {}),
            'class': 'jscolor',
            'maxlength': '6'
        }
        super().__init__(attrs=attrs)

    @property
    def media(self):
        """
        ColorPickerInput's Media
        """
        return Media(
            js=['admin/colorpicker/jscolor.min.js']
        )
