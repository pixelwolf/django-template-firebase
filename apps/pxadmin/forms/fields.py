# coding: utf-8
from urllib.parse import urljoin

from django import forms
from django.core.exceptions import ImproperlyConfigured
from apps.pxadmin.forms.widgets import PartialUrlInput, ColorPickerInput


class PartialUrlField(forms.URLField):
    widget = PartialUrlInput

    def __init__(self, root_url=None, **kwargs):
        super().__init__(**kwargs)

        # define the root url for the component.
        self.root_url = root_url
        self.widget.root_url = root_url

    def get_root_url(self, value):
        """
        Try to return a valid root url.
        """
        if not self.root_url:
            raise ImproperlyConfigured('The \'root_url\' must be provided to use PartialUrlField.')

        return self.root_url

    def to_python(self, value):
        if not value:
            return None

        # join the url with the url part.
        url = urljoin(self.get_root_url(value), value)

        # continue to base cleaner.
        return super(PartialUrlField, self).to_python(url)


class ColorPickerField(forms.CharField):
    """
    Render a color picker input
    """

    widget = ColorPickerInput

    def __init__(self, **kwargs):
        super().__init__(**{
            **kwargs,
            'max_length': 6
        })

    def clean(self, value):
        value = super().clean(value)

        if not value:
            return None
        # Add # to the color value
        return '#%s' % value.lstrip('#')
