# coding: utf-8
import time

from django.core.management.base import BaseCommand

from apps.pxadmin.styles import compile_scss


class Command(BaseCommand):
    help = 'Compile a SCSS file into a css file.'

    def add_arguments(self, parser):
        """
        Configures the command arguments.
        """
        parser.add_argument(
            '-s', '--source', dest='source',
            help='The scss file to be compiled. Default None.')

        parser.add_argument(
            '-d', '--destination', dest='destination',
            help='The name of the destination file. Default: None.')

        parser.add_argument(
            '-f', '--format', dest='format',
            choices=['nested', 'expanded', 'compact', 'compressed'],
            help='The format to compile the css file. Default: compressed.')

    def handle(self, *args, **options):
        # starts the package
        start = time.time()

        # Compile the css file throughout the scss file
        compile_scss(
            source_path=options.get('source'),
            output_path=options.get('destination'),
            output_style=options.get('format') or 'compressed',
        )

        # finish the package
        final = time.time()
        self.stdout.write('execution finished in %.2fs' % (final - start))
