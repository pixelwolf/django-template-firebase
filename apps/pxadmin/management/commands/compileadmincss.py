# coding: utf-8
import time

from django.conf import settings
from django.core.management.base import BaseCommand

from apps.pxadmin.styles import compile_scss


class Command(BaseCommand):
    help = 'Compile the admin SCSS file into a css file.'

    def handle(self, *args, **options):
        # starts the package
        start = time.time()

        # override variables
        variables = getattr(settings, 'ADMIN_SITE_STYLE_VARIABLES', None)

        # Compile the css file throughout the scss file
        compile_scss(
            source_path='apps/pxadmin/static/admin/css/scss/styles.scss',
            output_path='apps/pxadmin/static/admin/css/styles.min.css',
            output_style='compressed',
            variables=variables
        )

        # finish the package
        final = time.time()
        self.stdout.write('execution finished in %.2fs' % (final - start))
