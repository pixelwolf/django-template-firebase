# coding: utf-8
import os
import sass

from django.conf import settings


class SCSS:
    """
    A helper class to build a css file throughout a scss.
    """
    variables_section = '/* __variables__ */'

    def __init__(self, path):
        self.path = path

    @property
    def content(self):
        """
        Returns the SCSS file content.
        """
        if not hasattr(self, '_cached_content'):
            # open the scss file.
            scss = open(self.path, 'r')

            # store the content in instance variable.
            self._cached_content = scss.read()

            # close the file.
            scss.close()

        return self._cached_content

    def render_to_string(self, variables=None, output_style='compressed'):
        variables = variables or {}

        # get path of the file.
        path, _ = os.path.split(self.path)

        # replace the variables to provided values
        content = self.content.replace(
            self.variables_section, '\n'.join([
                '${}: {};'.format(key, val) for key, val in variables.items()
            ]))

        # compile the sass into css.
        return sass.compile(
            string=content,
            include_paths=(path,),
            output_style=output_style
        ).encode('utf-8')

    def compile(self, output_path, variables=None, output_style='compressed'):
        """
        Compile a scss to a destination file to be
        used in a website.
        """
        output_folder, _ = os.path.split(output_path)

        if not os.path.exists(output_folder):
            # grant that the output folder exists
            # before compile the css.
            os.makedirs(output_folder)

        # compile css into a string
        css = self.render_to_string(variables, output_style=output_style)

        with open(output_path, 'w') as f:
            # write the compiled css into the
            # destination file.
            f.write(css.decode("utf-8"))


def compile_scss(source_path, output_path=None, variables=None, output_style=None):
    """
    This function can be used as a shortcut to build a
    SCSS file and save into a file to be
    used in a website.
    """
    if not output_path:
        # in case of destination is not defined, try to build a path.
        path, filename = os.path.split(source_path)

        # get the filename without the extension
        filename, _ = os.path.splitext(filename)

        # get the file extension
        ext = '.min.css' if output_style == 'compressed' else '.css'

        # build the destination path on the same folder
        output_path = os.path.join(path, filename + ext)

    # apply the absolute path to the paths
    source_path = os.path.join(settings.BASE_DIR, source_path)
    output_path = os.path.join(settings.BASE_DIR, output_path)

    # get a SCSS instance
    scss = SCSS(source_path)

    # try to compile scss file from css
    scss.compile(
        output_path=output_path, variables=variables,
        output_style=output_style)
