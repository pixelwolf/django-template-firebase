# coding: utf-8
from django.conf import settings
from django.contrib import admin
from django.urls import path

from apps.pxadmin.utils import admin_url_pattern

admin.site.site_title = getattr(settings, "ADMIN_SITE_TITLE", "Administração da Aplicação")
admin.site.site_header = getattr(settings, "ADMIN_SITE_HEADER", "Administração da Aplicação")
admin.site.index_title = getattr(settings, "ADMIN_SITE_INDEX_TITLE", "Administração da Aplicação")
admin.site.site_url = getattr(settings, 'SITE_URL', None)


urlpatterns = [
    path(admin_url_pattern(), admin.site.urls),
]
