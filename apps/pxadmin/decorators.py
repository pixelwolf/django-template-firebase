# coding: utf-8


def admin_list_display(short_description, boolean=False, order_field=None):

    def wrap(func):
        """
        Wraps the function to add meta information
        to resolve a list display property
        on admin site.
        """
        func.short_description = short_description
        func.boolean = boolean
        func.admin_order_field = order_field
        return func
    return wrap


def admin_view(method=None, route=None, name=None, detail=True):

    def wrap(func):
        """
        Wraps the function to add meta information
        to resolve the view and register
        the url.
        """
        func.name = name
        func.route = route
        func.detail = detail
        func.is_extra_view = True

        return func

    if callable(method):
        return wrap(method)

    return wrap


def admin_changelist_widget(method=None, title=None, template=None):

    def wrap(func):
        """
        Add required meta information.
        """
        func.title = title
        func.template = template
        func.is_changelist_widget = True
        return func

    if callable(method):
        return wrap(method)

    return wrap


def admin_action(short_description=None):

    def wrap(func):
        """
        Wraps the function to add meta information
        to admin action.
        """
        name = func.__name__.replace('_action', '').replace('_', ' ')
        func.short_description = short_description or name
        func.is_extra_action = True
        return func
    return wrap
