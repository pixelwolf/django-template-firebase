# coding: utf-8
from urllib.parse import urlparse, parse_qs, urlunparse, urlencode

from django.conf import settings
from django.utils.http import urlunquote
from django.utils.module_loading import import_string


def querystring(url, includes=None, excludes=None):
    """
    Handle urls adding or excluding querystring parameters.
    """
    url = urlparse(url)
    query = parse_qs(url.query)
    includes = includes or {}
    excludes = excludes or []

    # apply includes
    query.update(includes)

    # apply excludes
    query = {key: value for key, value in query.items() if key not in excludes}

    # update url
    url = urlunparse(url._replace(query=urlencode(query, True)))

    return urlunquote(url)


def call_from_string(path, *args, **kwargs):
    """
    Returns a result from a function.
    """
    try:
        # import provided path
        result = import_string(path)

        if callable(result):
            # try to call the function and
            # return the result.
            return result(*args, **kwargs)

        return result
    except ImportError:
        return False


def admin_url_pattern(path=None):
    """
    Returns admin url based on settings.
    """
    output = [getattr(settings, 'ADMIN_SITE_URL', '/pxadmin/').rstrip('/').lstrip('/')]

    if path:
        output.append(path.rstrip('/').lstrip('/'))

    return '%s/' % '/'.join(output)
