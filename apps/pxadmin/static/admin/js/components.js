/* ==========================================================================
 * Dropdown.js
 * ==========================================================================
 * Copyright 2019 Pixelwolf.
 * Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)
 * ========================================================================== */

+function ($) {
    'use strict';

    // - Dropdown
    //
    //## Dropdown definition.

    $(window).on('load', function () {

        $('.dropdown [data-toggle="dropdown"]').click(function() {
            $(this).next('.dropdown-menu').toggle();
            return false;
        });

        $(document).click(function(e) {
            if (!$(e.target).is('.dropdown') && !$(e.target).parents().is('.dropdown')) {
                $('.dropdown-menu').hide();
            }
        });

    })

}(jQuery);

