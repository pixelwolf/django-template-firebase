# coding: utf-8
from django.contrib import admin

from apps.pxadmin.mixins import AdminActionMixin, AdminToolsMixin, AdminViewMixin


class SmartModelAdmin(AdminActionMixin, AdminToolsMixin, AdminViewMixin, admin.ModelAdmin):
    """
    Admin class that provides Custom Admin, Custom Action and Extra Tools.
    """
