# coding: utf-8
import os

from django.core.exceptions import ImproperlyConfigured
from django.core.files.storage import default_storage
from django.utils import six
from django.utils.deconstruct import deconstructible


@deconstructible
class UploadToDispatcher:
    """
    Dispatch function to allow overriding of `_upload_to` method.
    """
    def __init__(self, func_name):
        self.func_name = func_name

    def __call__(self, instance, filename):
        try:
            callable_func = getattr(instance, self.func_name)
        except AttributeError:
            raise ImproperlyConfigured(
                'Models where you can upload must have a '
                'method to retrieve the file name.')

        return callable_func(filename)


def upload_file(path_or_stream, upload_path, storage=None):
    """
    Upload file to another path and returns the file url.
    """
    # Define default storage for saving images on disk
    storage = storage or default_storage

    if isinstance(path_or_stream, six.string_types):
        # open the file based on file path
        content = open(path_or_stream, 'rb').read()

    else:
        # get the file upstream
        content = path_or_stream

    # send file to storage
    return storage.save(name=upload_path, content=content)
