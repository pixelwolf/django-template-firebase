# coding: utf-8
from packaging.version import Version as BaseVersion, InvalidVersion


class Version(BaseVersion):

    def _compare(self, other, method):
        if not isinstance(other, BaseVersion):
            try:
                other = Version(other)

            except (TypeError, InvalidVersion):
                return False

        return super()._compare(other, method)
