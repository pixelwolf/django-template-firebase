# coding: utf-8


def _to_bool(value):
    value = str(value).lower()

    if value in ('true', 't', '1'):
        return True

    if value in ('false', 'f', '0'):
        return False

    return None


def parse(value, cast):
    if cast == bool:
        # cast value to boolean.
        return _to_bool(value)

    try:
        value = cast(value)

    except (ValueError, TypeError):
        return None

    else:
        return value
