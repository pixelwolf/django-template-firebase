# coding: utf-8
import collections.abc


class ObjectDict:
    """
    Gives a simple dict an object behavior.
    """
    def __init__(self, mapping):
        self._data = mapping

    def __repr__(self):
        return f'ObjectDict({self._data})'

    @classmethod
    def _get_attr(cls, mapping, item):
        value = mapping[item]

        if isinstance(value, collections.abc.Mapping):
            value = cls(value)

        elif isinstance(value, collections.abc.Sequence):
            value = [cls(item) if isinstance(item, collections.abc.Mapping) else item for item in value]

        return value

    def __getattr__(self, item):
        """
        Tries to return a object attribute or a object key.
        """
        try:
            return getattr(self._data, item)

        except AttributeError:
            return ObjectDict._get_attr(self._data, item)

    def __getitem__(self, item):
        return self._get_attr(self._data, item)


def chunks(iterable, n):
    """
    Yield successive n-sized chunks from l.
    """
    for i in range(0, len(iterable), n):
        yield iterable[i: i + n]
