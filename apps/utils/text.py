# coding: utf-8
import random
import string
import unicodedata


def strip_accents(s):
    """
    Remove all accents from string.

    Args:
        s (str, required): String to remove accents.

    Returns:
        str
    """
    s = unicodedata.normalize('NFKD', s)

    # returns string without accents.
    return s.encode('ascii', 'ignore').decode('utf-8')


def random_password(length=6, allowed_chars=(string.ascii_uppercase + string.digits)):
    """
    Generate a random password with a defined length
    based on allowed characters.

    Args:
        length (int, optional): Password length. Default: 6.
        allowed_chars (str, optional): Password allowed characters. Default ASCII chars + digits.
    """
    if length <= 0:
        raise ValueError('Password length must be greater than 0.')

    return ''.join(random.choice(allowed_chars) for __ in range(length))


def plural(str_singular, str_plural, count):
    """
    Returns a pluralized string based on a counter.

    Args:
        str_singular (str, required): Singular string.
        str_plural (str, required): Plural string.
        count (int, required): Counter.

    Returns:
        str
    """
    return str_singular if count == 1 else str_plural
