# coding: utf-8
from django.db import models
from django.db.models import QuerySet
from django.db.models.manager import BaseManager
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _


class SoftDeleteQueryset(QuerySet):
    """
    Custom queryset to change the default
    delete behavior.
    """

    def delete(self):
        """
        Disable the bulk deletion behavior, and flags the
        objects as deleted instead.
        """
        self.update(deleted=True, deleted_at=timezone.localtime())


class SoftDeleteManager(BaseManager.from_queryset(SoftDeleteQueryset)):
    """
    Custom manager to handle soft delete behavior.
    """

    @property
    def active(self):
        """
        Queryset special function to return only
        not deleted objects.
        """
        queryset = self.get_queryset()

        # returns only active objects.
        return queryset.exclude(deleted=True)

    @property
    def deleted(self):
        """
        Queryset special function to return only
        deleted objects.
        """
        queryset = self.get_queryset()

        # returns only deleted objects
        return queryset.filter(deleted=True)


class SoftDeleteMixin(models.Model):

    deleted = models.BooleanField(
        _('Deleted'), editable=False,
        default=False)

    deleted_at = models.DateTimeField(
        _('Deletion Date'), editable=False,
        null=True, default=None)

    # IMPORTANT: Django ORM allows to delete objects through a queryset,
    # that's why is necessary to disable this behavior to prevent object
    # deletion by Django administration.
    objects = SoftDeleteManager()

    def delete(self, using=None, keep_parents=False):
        """
        When model is defined to delete objects softly,
        this method is no longer available.

        The object must be flagged as deleted instead.
        """
        self.deleted = True
        self.deleted_at = timezone.localtime()
        self.save()

    class Meta:
        abstract = True
