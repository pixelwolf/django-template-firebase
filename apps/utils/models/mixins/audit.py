# coding: utf-8
from django.db import models
from django.utils.translation import ugettext_lazy as _


class AuditMixin(models.Model):

    created_at = models.DateTimeField(
        _('Created At'), auto_now_add=True)

    updated_at = models.DateTimeField(
        _('Last Update'), auto_now=True)

    class Meta:
        abstract = True
