# coding: utf-8
from django.db import models
from django.utils import timezone

from apps.utils.models.mixins.audit import AuditMixin
from apps.utils.models.mixins.softdelete import SoftDeleteMixin


class QuerySet(models.QuerySet):
    """
    A queryset to provide the ability to soft deletion.
    """

    def delete(self):
        """
        Do not delete objects on the hard way,
        just update the deletion attributes.
        """
        assert self.query.can_filter(), \
            "Cannot use 'limit' or 'offset' with delete."

        if self._fields is not None:
            raise TypeError("Cannot call delete() after .values() or .values_list()")

        del_query = self._chain()

        # update current query
        self.delete_queryset(del_query)

        # simulate a query deletion
        return True, del_query.count()

    def delete_queryset(self, queryset, **kwargs):
        """
        Apply the deletion to queryset avoiding hard deletion.
        """
        queryset.update(
            is_deleted=True,
            deleted_at=timezone.now(),
            **kwargs
        )


class DefaultManager(models.Manager.from_queryset(QuerySet)):
    """
    A manager to filter only active objects on model.
    """

    def get_queryset(self):
        """
        Returns only objects not deleted.
        """
        queryset = super(DefaultManager, self).get_queryset()
        return queryset.filter(is_deleted=False)


class NotAllowedDeleteQuerySet(models.QuerySet):
    """
    A queryset to provide the ability to soft deletion.
    """

    def delete(self):
        """
        Do not allow to delete with this manager.
        """
        raise PermissionError('You are not allowed to perform this action through this manager.')


class SoftDeleteManager(models.Manager.from_queryset(NotAllowedDeleteQuerySet)):
    """
    A manager that provides the ability to handle
    only deleted objects.
    """

    def get_queryset(self):
        """
        Returns only objects not deleted.
        """
        return super(SoftDeleteManager, self).get_queryset().filter(is_deleted=True)


class AbstractBaseModel(AuditMixin, SoftDeleteMixin, models.Model):
    """
    A Base class tha provides the audit fields
    and is soft delete based.

    Usage:

    To implement this is quite simple, it is only
    necessary to put the ``AbstractBaseModel``
    instead ``models.Model``.

    >>> from apps.utils.models.base import AbstractBaseModel
    >>>
    >>> class Foo(AbstractBaseModel):
    >>>     ...
    >>>
    """

    # Force to handle only active objects by default.
    objects = DefaultManager()

    # Provide the ability to handle deleted objects.
    deleted = SoftDeleteManager()

    class Meta:
        abstract = True
