# coding: utf-8
from django.core.exceptions import ImproperlyConfigured
from django.db.models import Q

from apps.utils.parsing import parse


INVALID_VALUE = '__INVALID__'


class BaseFilter:
    """
    Define the filter object with expected features.
    """
    def __init__(self, url_kwarg):
        if not url_kwarg:
            raise ImproperlyConfigured(
                '"url_kwarg" is required on class %s.' %
                self.__class__.__name__)

        self.url_kwarg = url_kwarg

    def value(self, request):
        """
        Returns the value from lookup kwarg.
        """
        return request.GET.get(self.url_kwarg, None)

    def filter(self, request, queryset, view):
        """
        This method must implement a way to handle the queryset
        and apply the filter.

        Args:
            request: (view.Request, required) - The view request.
            queryset: (models.Queryset, required) - Queryset to be filtered.
            view: (View, required) - The view that is handling the queryset.

        Returns:
            Queryset object.
        """
        raise NotImplementedError('The "filter" method must be implemented.')

    def __call__(self, request, queryset, view):
        return self.filter(request, queryset, view)


class SimpleFilter(BaseFilter):
    """
    Simple filter based on a single value.
    """
    def __init__(self, url_kwarg, lookup_filter=None):
        super().__init__(url_kwarg)
        self.lookup_filter = lookup_filter or url_kwarg

    def filter(self, request, queryset, view):
        """
        Filter the queryset based on a single value from url.
        """
        value = self.value(request)

        if not value == INVALID_VALUE:
            queryset = queryset.filter(**{self.lookup_filter: value})

        return queryset


class BooleanFilter(SimpleFilter):
    """
    A filter to handle boolean values into queryset.
    """

    def value(self, request):
        """
        Cast value to boolean.
        """
        value = super().value(request)

        # cast value or return INVALID_VALUE flag.
        return parse(value, cast=bool) or INVALID_VALUE


class IntegerFilter(SimpleFilter):
    """
    A filter to handle integer values into queryset.
    """

    def __init__(self, url_kwarg, lookup_filter=None, min_value=None, max_value=None):
        super().__init__(url_kwarg, lookup_filter=lookup_filter)
        self.min_value = min_value
        self.max_value = max_value

    def value(self, request):
        """
        Cast value to int and apply min and max validations.
        """
        value = super().value(request)

        # parse value to int.
        value = parse(value, cast=int)

        if not value or \
                (self.max_value and value > self.max_value) or \
                (self.min_value and value < self.min_value):
            return INVALID_VALUE

        return value


class EnumFilter(BaseFilter):
    """
    Filters values based on a value dict.
    """
    def __init__(self, url_kwarg, lookup_filter=None, keymap=None):
        super().__init__(url_kwarg)

        if not keymap:
            # do not accept this filter without a keymap.
            raise ImproperlyConfigured(
                'The "keymap" argument is required on class %s.' % self.__class__.__name__
            )

        self.keymap = keymap
        self.lookup_field = lookup_filter or url_kwarg

    def filter(self, request, queryset, view):
        """
        Filter the queryset based on a single value from
        url located on a keymap.
        """
        filter_key_value = request.GET.get(self.url_kwarg)
        filter_value = self.keymap.get(filter_key_value, None)

        if filter_value is not None:
            queryset = queryset.filter(**{self.lookup_field: filter_value})

        return queryset


class SimpleSearchFilter(BaseFilter):
    """
    Activate the search feature in the view.
    """
    def __init__(self, url_kwarg, fields=None, lookup_suffix='icontains'):
        super().__init__(url_kwarg)

        if not fields or not isinstance(fields, (list, tuple)):
            raise ImproperlyConfigured(
                'The \'fields\' argument is required and must be a list of names.'
            )

        self.fields = fields
        self.lookup_suffix = lookup_suffix

    def get_lookup_fields(self):
        """
        Walk into fields and grant that the field will
        be on the correct django lookup format.

        Possible values for lookups:
            - contains, icontains
            - startswith, istartswith
            - endswith, iendswith
            - exact
            - None
        """
        if not self.lookup_suffix:
            # if there is no suffix to lookups just
            # returns the fields.
            return self.fields

        return ['{field}__{lookup}'.format(
            field=field,
            lookup=self.lookup_suffix
        ) for field in self.fields]

    def filter(self, request, queryset, view):
        """
        Apply the search using all defined fields.
        """
        value = request.GET.get(self.url_kwarg)

        if not value:
            # if there is no value to be searched
            # just ignore the filter.
            return queryset

        lookups = Q()

        for lookup in self.get_lookup_fields():
            # concat all filters using OR condition
            # and returns a valid lookup
            # for queryset.
            lookups |= Q(**{lookup: value})

        # returns filtered queryset.
        return queryset.filter(lookups)
