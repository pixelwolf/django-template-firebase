# coding: utf-8
from django.db.models import QuerySet
from rest_framework.pagination import PageNumberPagination as BasePageNumberPagination


class PageNumberPagination(BasePageNumberPagination):
    """
    A custom page number paginator to enable the option to `page_size` param.
    """
    # Client can control the page size using this query parameter.
    # Default is 'None'. Set to eg 'page_size' to enable usage.
    page_size_query_param = 'page_size'

    # Set to an integer to limit the maximum page size the client may request.
    # Only relevant if 'page_size_query_param' has also been set.
    max_page_size = None

    # Enable to return all items in database. It must be used carefully to not
    # overload the server.
    ALL = 'ALL'

    def get_page_size(self, request):
        """
        Grant ability to return the whole database items by
        passing page_size=ALL in querystring url.
        """
        page_size = request.query_params.get(self.page_size_query_param)

        _total_count = getattr(self, '_total_count')

        if page_size == PageNumberPagination.ALL and _total_count != 0:
            # returns all queryset items.
            return _total_count

        if page_size == PageNumberPagination.ALL and _total_count == 0:
            # only returns the default page_size when
            # queryset is empty.
            return self.page_size

        # otherwise return the base page_size.
        return super().get_page_size(request)

    def paginate_queryset(self, queryset, request, view=None):
        _total_count = queryset.count() if isinstance(queryset, QuerySet) else len(queryset)

        # define the total count queryset to use
        # in page_size processor.
        setattr(self, '_total_count', _total_count)

        # continue to paginate queryset.
        return super().paginate_queryset(queryset, request, view=view)
