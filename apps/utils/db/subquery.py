# pylint: disable=W0223
from django.db import models
from django.db.models import Subquery


class SubqueryCount(Subquery):
    template = '(SELECT COUNT(%(by_field)s) FROM (%(subquery)s) AS "_count")'
    output_field = models.IntegerField()

    def __init__(self, queryset, output_field=None, by_field=None, **extra):
        by_field = by_field or 'id'

        # optimize query by getting only required field to count.
        queryset = queryset.values(by_field)

        # continue to super class.
        super().__init__(queryset, output_field=output_field, by_field=by_field, **extra)


class SubqueryExists(Subquery):
    template = '(SELECT COUNT(%(by_field)s) > 0 FROM (%(subquery)s) AS "_exists")'
    output_field = models.BooleanField()

    def __init__(self, queryset, output_field=None, by_field=None, **extra):
        by_field = by_field or 'id'

        # optimize query by getting only required field to count.
        queryset = queryset.values(by_field)

        # continue to super class.
        super().__init__(queryset, output_field=output_field, by_field=by_field, **extra)
