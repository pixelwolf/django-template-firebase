# coding: utf-8
from collections import OrderedDict
from django.db import connections, DEFAULT_DB_ALIAS


class DatabaseWrapper:

    default_db_alias = DEFAULT_DB_ALIAS

    def get_connection(self, using=None):
        """
        Returns a a database connection which can be used
        to execute scripts direct to the a database.
        """
        return connections[using or self.default_db_alias]

    def get_cursor(self, using=None):
        """
        Returns a cursor based on a connection.
        """
        connection = self.get_connection(using=using)
        return connection.cursor()

    def select(self, query, params=None, as_dict=False, using=None):
        """
        Executes a query on a database that will return one information,
        it was can be used to execute SELECT statements or any statement
        which returns any values from a database.
        """
        cursor = self.get_cursor(using=using)
        cursor.execute(query.format(**(params or {})))

        # Get all returned data
        data = cursor.fetchall()

        if as_dict is True:
            columns = [col[0] for col in cursor.description]
            return [OrderedDict(zip(columns, row)) for row in data]

        return data

    def select_one(self, query, params=None, using=None):
        """
        Executes a query on a database that will return one information,
        it was can be used to execute SELECT statements or any statement
        which returns a value from a database.
        """
        cursor = self.get_cursor(using=using)
        cursor.execute(query.format(**(params or {})))

        # Get the returned data
        data = cursor.fetchone()

        return data[0]

    def execute(self, query, params=None, using=None):
        """
        Executes any query on a database and it does not return anything.
        """
        cursor = self.get_cursor(using=using)
        cursor.execute(query.format(**(params or {})))


db = DatabaseWrapper()
