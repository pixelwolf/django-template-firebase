# coding: utf-8
from django.db.models import Func
from django.db.models import FloatField


class SimilarityRatio(Func):
    """
    Custom function that compare two strings
    to generate a similarity ratio between
    them.
    """
    function = 'SIMILARITY_RATIO'

    def __init__(self, *expressions, output_field=None, **extra):
        # Ensure that will always return a
        # float field.
        output_field = FloatField()

        super().__init__(*expressions, output_field=output_field, **extra)
