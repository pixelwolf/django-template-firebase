# coding: utf-8


class ContextFormMixin:

    def __init__(self, *args, **kwargs):
        self.context = kwargs.pop('context', None) or {}
        super().__init__(*args, **kwargs)
