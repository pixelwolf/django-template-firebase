# coding: utf-8
import base64

from Crypto import Random
from django.conf import settings
from Crypto.Cipher import AES


BS = 16
pad = (lambda s: s + (BS - len(s) % BS) * chr(BS - len(s) % BS))
unpad = (lambda s: s[0:-s[-1]])


class Crypt:

    def __init__(self, secret):
        self.secret = secret[:32]
        self.mode = AES.MODE_CBC

    def encrypt(self, raw):
        raw = pad(raw)
        iv = Random.new().read(AES.block_size)
        cipher = AES.new(self.secret, AES.MODE_CBC, iv)
        return base64.b64encode(iv + cipher.encrypt(raw))

    def decrypt(self, enc):
        enc = base64.b64decode(enc)
        cipher = AES.new(self.secret, AES.MODE_CBC, enc[:16])
        return unpad(cipher.decrypt(enc[16:]))


# Define a default crypt instance.
crypt = Crypt(getattr(settings, 'SECRET_KEY'))
