# coding: utf-8
from django.utils.translation import ugettext_lazy as _

from rest_framework import exceptions
from rest_framework.versioning import BaseVersioning

from apps.utils.versioning import Version


class HttpHeaderAPIVersioning(BaseVersioning):
    """
    Uses XApiVersion header to define versions.

    GET /something/ HTTP/1.1
    Host: example.com
    XApiVersion: 1.0.0
    """
    invalid_version_message = _('Invalid version in "XApiVersion" header.')

    def is_allowed_version(self, version):
        """
        Define whether the version is valid or not.
        """
        return True

    def determine_version(self, request, *args, **kwargs):
        version = request.META.get('HTTP_X_API_VERSION')
        version = Version(version) if version else None

        if not self.is_allowed_version(version):
            raise exceptions.NotAcceptable(self.invalid_version_message)

        return version
