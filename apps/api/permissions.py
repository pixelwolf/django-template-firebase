# coding: utf-8
from django.conf import settings
from django.utils.translation import ugettext_lazy as _

from rest_framework.authentication import get_authorization_header
from rest_framework.permissions import IsAuthenticated


class IsPublic(IsAuthenticated):
    """
    Allow access to anonymous users with public token or
    authenticated users.

    Example: Authorization: Basic {token}
    """
    keyword = b'Basic'
    authorized_token = str(getattr(settings, 'PUBLIC_API_ACCESS_TOKEN')).encode()

    default_error_messages = {
        'invalid_header': _('Invalid authorization header.'),
        'invalid_token': _('Invalid authorization token.'),
    }

    message = default_error_messages['invalid_token']

    def has_permission(self, request, view):
        """
        Check the authorization header to get the generic
        access token and validate it.
        """
        if super().has_permission(request, view):
            # grant access if user is authenticated.
            return True

        try:
            keyword, token = get_authorization_header(request).split()

        except ValueError:
            self.message = self.default_error_messages['invalid_header']
            return False

        else:
            if not self.keyword.lower() == keyword.lower():
                self.message = self.default_error_messages['invalid_header']
                return False

            if not token == self.authorized_token:
                self.message = self.default_error_messages['invalid_token']
                return False

        return True
