# coding: utf-8
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.api.permissions import IsPublic


class IndexView(APIView):
    permission_classes = [IsPublic]

    def get(self, request):
        return Response({'message': 'Index view.'}, status=status.HTTP_200_OK)
