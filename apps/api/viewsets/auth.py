# coding: utf-8
from rest_framework import status
from rest_framework.response import Response
from rest_framework.viewsets import ViewSet

from apps.api.serializers.auth import AuthTokenSerializer


class AuthenticationView(ViewSet):
    serializer_class = AuthTokenSerializer
    authentication_classes = []
    permission_classes = []

    def post(self, request, *args, **kwargs):
        """
        Login to firebase by passing email and password to retrieve a valid token.
        Commonly used to authenticate Postman and use api.
        """
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        return Response(
            data={'token': serializer.validated_data['token']},
            status=status.HTTP_200_OK)
