# coding: utf-8
from django.urls import path, include

from apps.api.viewsets.index import IndexView

urlpatterns = [
    path('', IndexView.as_view())
]
