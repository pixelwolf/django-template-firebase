# coding: utf-8
from django.urls import path, include


app_name = 'api'


urlpatterns = [
    path('auth/', include('apps.api.urls.auth')),
    path('index/', include('apps.api.urls.index'))
]
