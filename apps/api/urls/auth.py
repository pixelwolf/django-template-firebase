# coding: utf-8
from django.urls import path, include
from rest_framework import routers
from apps.api.viewsets.auth import AuthenticationView


router = routers.DefaultRouter()
router.register(r'', AuthenticationView, basename='auth')


urlpatterns = [
    path('', include(router.urls)),
]
