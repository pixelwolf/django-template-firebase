# coding: utf-8

from django.utils.translation import ugettext_lazy as _
from rest_framework import serializers

from modules.firebase.auth.client import client, FirebaseError


class AuthTokenSerializer(serializers.Serializer):
    """
    Authenticate with firebase by using email and password
    and returns a valid token and user information.
    """
    email = serializers.CharField(label=_("email"))

    password = serializers.CharField(
        label=_("password"),
        style={'input_type': 'password'},
        trim_whitespace=False)

    def validate(self, attrs):
        email, password = attrs.get('email'), attrs.get('password')

        if not all([email, password]):
            raise serializers.ValidationError({
                'email': _('Must include "username" and "password".')
            }, code='auth')

        try:
            token = client.login(email, password)

        except FirebaseError:
            raise serializers.ValidationError({
                'email': _('Unable to log in with provided credentials.')
            }, code='auth')

        else:
            return dict(attrs, token=token)

    def update(self, instance, validated_data):
        """
        Ignore update method.
        """

    def create(self, validated_data):
        """
        Ignore create method.
        """