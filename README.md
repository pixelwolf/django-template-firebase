# Django Rest Project Template

This is a simple and quick project to help you to start a django project with a rest api.

The DjangoRestProjectTemplate project builds on Django 2.0 and rest-framework. This makes it easier to understand how your project needs to be build and guide you with all things already installed and configured. It includes a user token authentication and a very clean project structure.

## Setup

Make sure that the [Python3.5](https://www.python.org/), [Virtualenv](https://virtualenv.pypa.io/en/stable/), [Fabric==1.14.0](http://www.fabfile.org/) and [Django](https://www.djangoproject.com/) are installed in your computer before starts here.

- The first step is create a local project using this awesome template:

```
$ django-admin.py startproject --template=https://bitbucket.org/pixelwolf/django-template-firebase/get/1.0.0.zip --extension=py,md,ini,env,template <PROJECTNAME>
```

- Second you will need to set the environment:

```
$ cd project_folder

$ mkvirtualenv <PROJECTNAME> -p /usr/bin/python3 -a $(pwd)

$ export VIRTUALENV_HOME=/path/to/my/custom/virtualenv/container/folder/
```
- Then you will need to setup this project:

```
$ cd project_folder

$ docker-compose up postgres

$ fab localhost provision

```

- If all things are right access the path [http://localhost:8000/](http://localhost:8000/) on your browser to access your project.
