# coding: utf-8
import configparser


class ConfigFileSection:
    _cached_items = None

    def __init__(self, name, parser):
        self._section_name = name
        self._parser = parser

    def __repr__(self):
        return '<Configuration Section: %s>' % self._section_name

    def __dict__(self):
        return self._items

    def __iter__(self):
        for key, value in self._items:
            yield key, value

    def __getattr__(self, item):
        try:
            return super(ConfigFileSection, self).__getattribute__(item)
        except AttributeError:
            return self._items.get(item)

    @property
    def _items(self):
        if not self._cached_items:
            items = self._parser.items(self._section_name)
            self._cached_items = dict(items or {})
        return self._cached_items


class ConfigFile:
    _cached_parser = None

    def __init__(self, path):
        self.path = path

        self.sections = dict([(
            section, ConfigFileSection(section, self.parser)
        ) for section in self.parser.sections()])

    @property
    def parser(self):
        if not self._cached_parser:
            self._cached_parser = configparser.ConfigParser()
            self._cached_parser.read(self.path)
        return self._cached_parser

    def __getattr__(self, item):
        try:
            return super(ConfigFile, self).__getattribute__(item)
        except AttributeError:
            return self.sections.get(item)

    def __getitem__(self, item):
        return self.__getattr__(item)

    def __repr__(self):
        return '<Configuration: %s>' % self.path
